(function(){
    angular
        .module('app')
        .factory('dateFactory', dateFactory);

    function dateFactory(){
        var dateObj = {};

        dateObj.formatDateToDDMMYYYY = function (date) {
            var day = (date.getDate() >= 10) ? date.getDate() : '0' + date.getDate();
            var month = (date.getMonth() >= 9) ? date.getMonth() + 1 : "0" + (date.getMonth() + 1);
            var year = date.getFullYear();

            return (day + "/" + month + "/" + year);
        };

        return dateObj;
    }
})();