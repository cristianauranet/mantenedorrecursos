(function () {
    angular
        .module('app')
        .factory('editCountryFactory', editCountryFactory);

    editCountryFactory.$inject = ['$http', '$q', 'configConstant'];

    function editCountryFactory($http, $q, configConstant) {
        var editCountryObj = {};

        editCountryObj.editCountry = function (Idcountry) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_EDITCOUNTRY + Idcountry;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {

                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        editCountryObj.GetNivelPais = function (CodPais) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_EDITCOUNTRY + CodPais;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        editCountryObj.getNacionalidad = function (idSes) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_NATIONALITY + idSes;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        return editCountryObj;
    }
})();