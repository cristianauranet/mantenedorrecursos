(function () {
    angular
        .module('app')
        .factory('adjuntosFactory', adjuntosFactory);

    adjuntosFactory.$inject = ['$http', '$q', 'configConstant'];

    function adjuntosFactory() {
        var adjuntodObj = {};

        adjuntosObj.getFiles = function (idSes, RutProv, estadoSC, estadoPortal) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SEARCHQUOTATION + "?idSes=" + idSes + '&Rut=' + RutProv + '&EstadoSC=' + estadoSC + '&EstadoPortal=' + estadoPortal;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        adjuntosObj.editFiles = function (idSes, cliente, idCotERP, idCotPP) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLoginERP + configConstant.METHOD_EDITHEADCOTIZACION + "?idSes=" + idSes + '&cliente=' + cliente + '&IdSCotCabeza=' + idCotERP + '&IdSCotCabezaProv=' + idCotPP;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        adjuntosObj.saveFiles = function (idSession, cliente, cotizacion) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLoginERP + configConstant.METHOD_SAVEHEADCOTIZACION + cliente + '/' + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: cotizacion,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        adjuntosObj.deleteFiles = function (id, idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_DELETEUSER + id + "/" + idSession;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {

                        return res.data;
                    } else {
                        return deferred.reject(res.data);

                    }


                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        return adjuntosObj;
    }
})();