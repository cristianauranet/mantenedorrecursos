(function(){
    angular
        .module('app')
        .factory('stringFactory', stringFactory);

    function stringFactory(){
        var strObj = {};

        strObj.capitalizeString = function (str) {
            return (str.slice(0, 1).toUpperCase() + str.slice(1).toLowerCase());
        };

        return strObj;
    }
})();