(function () {
    angular
        .module('app')
        .factory('CountryFactory', CountryFactory);

    CountryFactory.$inject = ['$http', '$q', 'configConstant'];

    function CountryFactory($http, $q, configConstant) {
        var countryObj = {};

        countryObj.DevolverPais = function () {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_COUNTRY;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        countryObj.devolverNiveles = function (ubigeo) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_UBIGEO;
            return $http({
                    method: 'POST',
                    url: url,
                    data: ubigeo,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        countryObj.GetNivelPais = function (CodPais) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_EDITCOUNTRY + CodPais;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        return countryObj;
    }
})();