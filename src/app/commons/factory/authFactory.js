(function () {
    angular
        .module('app')
        .factory('authFactory', authFactory);

    authFactory.$inject = ['$http', '$q', 'configConstant'];

    function authFactory($http, $q, configConstant) {
        var authObj = {};

        authObj.loginFactory = function (userLogin) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_LOGIN;
            return $http({
                    method: 'POST',
                    url: url,
                    data: userLogin,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        authObj.registrarEmpresaRedSocial = function (userRed) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_REGISREDESOCIAES;
            return $http({
                    method: 'POST',
                    url: url,
                    data: userRed,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        authObj.getAllNamesOfRubro = function (rubro, idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SEARCHALLOFRUBRO + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: rubro,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        authObj.searchFilterMaquinas = function (idSession, filter) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SEARCHFILTERMAQUINAS + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: filter,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        authObj.searchFilterAnuncios = function (idSession, filter) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SEARCHFILTERANUNCIOS + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: filter,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {

                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        authObj.searchFullTextModelo = function (filter, idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_FULLTEXTTIPOMODELO + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: filter,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        authObj.ListTipoMarcaModeloMaquinarias = function (MaquinariasI) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETTIPOMARCAMODELO;
            return $http({
                    method: 'POST',
                    url: url,
                    data: MaquinariasI,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        authObj.getdataservicenew = function (idSession, obj) {
        };

        return authObj;
    }
})();