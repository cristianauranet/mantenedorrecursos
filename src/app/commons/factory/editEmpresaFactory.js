(function () {
    angular
        .module('app')
        .factory('editEmpresaFactory', editEmpresaFactory);

    editEmpresaFactory.$inject = ['$http', '$q', 'configConstant'];

    function editEmpresaFactory($http, $q, configConstant) {
        var editEmpresaObj = {};

        editEmpresaObj.editCompany = function (idCompany, idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_EDITCOMPANY + idCompany + "/" + idSession;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        editEmpresaObj.saveDatosEmpresa = function (idSession, datosemp) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SAVEEMPRESADATOS + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: datosemp,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        return editEmpresaObj;
    }
})();