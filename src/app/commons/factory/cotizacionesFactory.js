(function () {
    angular
        .module('app')
        .factory('cotizacionesFactory', cotizacionesFactory);

    cotizacionesFactory.$inject = ['$http', '$q', 'configConstant'];

    function cotizacionesFactory($http, $q, configConstant) {
        var cotizacionesObj = {};

        cotizacionesObj.getQuotation = function (idSes, RutProv, estadoSC, estadoPortal) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SEARCHQUOTATION + "?idSes=" + idSes + '&Rut=' + RutProv + '&EstadoSC=' + estadoSC + '&EstadoPortal=' + estadoPortal;
            //url =   url.toString().replace('%20', ' ');
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        cotizacionesObj.editHeadCotizacion = function (idSes, cliente, idCotERP, idCotPP) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLoginERP + configConstant.METHOD_EDITHEADCOTIZACION + "?idSes=" + idSes + '&cliente=' + cliente + '&IdSCotCabeza=' + idCotERP + '&IdSCotCabezaProv=' + idCotPP;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        cotizacionesObj.saveHeadCotizacion = function (idSession, cliente, cotizacion) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLoginERP + configConstant.METHOD_SAVEHEADCOTIZACION + cliente + '/' + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: cotizacion,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        cotizacionesObj.saveHeadCotizacionStatus = function (idSession, cotizacion) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SAVEHEADCOTIZACIONSTATUS + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: cotizacion,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        cotizacionesObj.saveConsumo = function (idSession, consumo) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SAVECONSUMO + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: consumo,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        cotizacionesObj.editDetailCotizacion = function (idSes, cliente, idCotERP, idCotPP) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLoginERP + configConstant.METHOD_EDITDETAILCOTIZACION + "?idSes=" + idSes + '&cliente=' + cliente + '&IdSCotCabeza=' + idCotERP + '&IdSCotCabezaProv=' + idCotPP;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        cotizacionesObj.getGlosas = function (id1, id2, idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETGLOSAS + id1 + "/" + id2 + "/" + idSession;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        cotizacionesObj.saveDetailCotizacion = function (idSession, cliente, detcotizacion) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLoginERP + configConstant.METHOD_SAVEDETAILCOTIZACION + cliente + '/' + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: detcotizacion,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        cotizacionesObj.getReportCotizacion = function (idSession, cliente, report) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLoginERP + configConstant.METHOD_GETREPORTOC + idSession + '/' + cliente;
            return $http({
                    method: 'POST',
                    url: url,
                    data: report,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        cotizacionesObj.getTarifaRecurso =  function (idSes, idRec, CodPais) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETTARIFARECURSO + "?idSes=" + idSes + '&idRec=' + idRec + '&CodPais=' + CodPais;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        cotizacionesObj.getSaldoCuentaCorriente = function (idSes, idPer) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETSALDOCTACORRIENTE + "?idSes=" + idSes + '&IdPer=' + idPer;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        return cotizacionesObj;
    }
})();