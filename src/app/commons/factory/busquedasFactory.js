(function () {
    angular
        .module('app')
        .factory('busquedasFactory', busquedasFactory);

    busquedasFactory.$inject = ['$http', '$q', 'configConstant'];

    function busquedasFactory($http, $q, configConstant) {
        var busquedasObj = {};

        busquedasObj.getAllNamesOfRubro = function (rubro, idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SEARCHALLOFRUBRO + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: rubro,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        busquedasObj.searchFilterProveedores = function (idSession, filter) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SEARCHFILTERPROVEEDORES + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: filter,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        busquedasObj.getGlosasDisponibilidad = function (idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETGLOSASDISPONIBILIDAD + "/" + idSession;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);

                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        busquedasObj.getOffice = function (id, idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETOFFICE + id + "/" + idSession;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        busquedasObj.ListTipoMarcaModeloMaquinarias = function (MaquinariasI) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETTIPOMARCAMODELO;
            return $http({
                    method: 'POST',
                    url: url,
                    data: MaquinariasI,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {

                        return res.data;
                    } else {
                        return deferred.reject(res.data);

                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        busquedasObj.editDatosUser = function (id, idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_EDITUSER + id + "/" + idSession;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        busquedasObj.getDatosExperiencia = function (idUser, idPer, idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETPOSTULANTCUR + idUser + "/" + idPer + "/" + idSession;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });

        };

        busquedasObj.getCurriculum = function (idUser, idPer, idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETPOSTULANT + idUser + "/" + idPer + "/" + idSession;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        return busquedasObj;
    }
})();