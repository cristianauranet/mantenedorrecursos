(function () {
    angular
        .module('app')
        .factory('utilityFactory', utilityFactory);

    utilityFactory.$inject = ['$http', '$q', 'configConstant'];

    function utilityFactory($http, $q, configConstant) {
        var utilityObj = {};

        utilityObj.getGlosas = function (id1, id2, idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETGLOSAS + id1 + "/" + id2 + "/" + idSession;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        utilityObj.sendEmailSoporte = function (idSession, soporte) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SENDEMAILSOPORTE + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: soporte,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        utilityObj.obtenerPasosModulo = function (idSession, mod) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETPASOSMODULOS + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: mod,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        utilityObj.obtenerPasosAll = function (idSession, mod) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETPASOSALL + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: mod,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        utilityObj.saveSoporteOnline = function (idSession, soporte) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SAVESOPORTONLINE + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: soporte,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        utilityObj.saveGlosaSugerida = function (idSession, glosa) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_ADDGLOSASUGERIDA + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: glosa,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        utilityObj.getAttached = function (idSession, idPer, idProv, doc) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETATTACHED + idSession + '/' + idPer + '/' + idProv + '/' + doc;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        utilityObj.loadAttached = function (idSession, id, idPer) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_LOADATTACHED + idSession + '/' + id + '/' + idPer;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        utilityObj.deleteAttached = function (idSession, id, idPer) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_DELETEATTACHED + idSession + "/" + id + "/" + idPer;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        utilityObj.saveAttached = function (idSession, file) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SAVEATTACHED + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: file,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        utilityObj.editGlosaToolTips = function (idSession, tool) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_EDITGLOSATOOLTIPS + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: tool,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        utilityObj.seeMailUser = function (idSession, user) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SEEMAILUSER + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: user,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        utilityObj.getServiceGenerico = function (idSession, sp) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_GETDATA + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: sp,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        return utilityObj;
    }
})();