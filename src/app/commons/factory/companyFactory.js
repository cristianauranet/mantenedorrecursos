(function () {
    angular
        .module('app')
        .factory('companyFactory', companyFactory);

    companyFactory.$inject = ['$http', '$q', 'configConstant'];

    function companyFactory($http, $q, configConstant) {
        var companyObj = {};

        companyObj.editCompany = function (idCompany, idSession) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_EDITCOMPANY + idCompany + "/" + idSession;
            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {
                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        companyObj.saveCompany = function (idSession, company) {
            var deferred;
            deferred = $q;
            var url = configConstant.URLLogin + configConstant.METHOD_SAVECOMPANY + idSession;
            return $http({
                    method: 'POST',
                    url: url,
                    data: company,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    responseType: 'json'
                })
                .then(function (res) {
                    if (typeof res.data === 'object') {

                        return res.data;
                    } else {
                        return deferred.reject(res.data);
                    }
                }, function (res) {
                    return deferred.reject(res.data);
                });
        };

        return companyObj;
    }
})();