(function () {
    angular
        .module('app')
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {
                var homeState = {
                    name: 'home',
                    url: '/home',
                    templateUrl: 'views/home/home.html',
                    controller: 'homeCtrl'
                };

                var loginState = {
                    name: 'login',
                    url: '/login',
                    templateUrl: 'views/login/login.html',
                    controller: 'loginCtrl'
                };

                $stateProvider.state(homeState);
                $stateProvider.state(loginState);

                $urlRouterProvider.otherwise('/login');
            }
        ]);
})();