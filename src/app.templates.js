angular.module('app').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/home/home.html',
    "<!DOCTYPE html>\r" +
    "\n" +
    "<div ng-controller=\"homeCtrl as vm\">\r" +
    "\n" +
    "    <h1>{{vm.title}}</h1>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('views/login/login.html',
    "<!DOCTYPE html>\r" +
    "\n" +
    "<div ng-controller=\"loginCtrl as vm\">\r" +
    "\n" +
    "    <h1>{{vm.title}}</h1>\r" +
    "\n" +
    "</div>"
  );

}]);
