var sass = require('node-sass');

module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        htmlhint: {
            all: ['src/**/*.html']
        },
        stylelint: {
            options: {
                configFile: 'etc/.stylelintrc'
            },
            sass: [
                'src/sass/*.scss'
            ],
            css: [
                'src/css/*.css'
            ]
        },
        jshint: {
            files: [
                'Gruntfile.js',
                'src/app.js',
                'src/**/*.js'
            ]
        },
        htmlmin: {
            dist: {
                options: {
                    compress: true,
                    collapseBooleanAttributes: true,
                    collapseWhitespace: true,
                    /* removeAttributeQuotes: true, */
                    removeComments: true,
                    removeEmptyAttributes: true,
                    removeRedundantAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true
                },
                src: 'src/index.html',
                dest: 'dist/index.html'
            }
        },
        ngtemplates: {
            app: {
                cwd: 'src/app',
                src: ['views/**/*.html', 'commons/templates/*.html'],
                dest: 'src/app.templates.js',
                options: {
                    module: 'app',
                    htmlmin: '<%= htmlmin.options %>'
                }
            }
        },
        copy: {
            libs: {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: [
                    'node_modules/bootstrap/dist/fonts/**',
                    'node_modules/simple-line-icons/fonts/**',
                    'src/fonts/**'
                ],
                dest: 'dist/fonts'
            }
        },
        sass: {
            options: {
                implementation: sass,
                sourceMap: true
            },
            dist: {
                src: ['src/sass/main.scss'],
                dest: 'src/css/main.css'
            }
        },
        cssmin: {
            options: {
                compress: true,
                removeComments: true,
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd/mm/yyyy") %> */\n'
            },
            libs: {
                src: [
                    'node_modules/bootstrap/dist/css/bootstrap.css',
                    'node_modules/bootstrap/dist/css/bootstrap-theme.css',
                    'node_modules/simple-line-icons/css/simple-line-icons.css',
                    // 'node_modules/@fortawesome/fontawesome-free/css/all.css',
                    'node_modules/angular-toastr/dist/angular-toastr.css',
                    'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
                    'node_modules/angular-xeditable/dist/css/xeditable.css',
                    'node_modules/@priotas/angular-loading-bar/src/angular-loading-bar.css',
                    'node_modules/normalize.css/normalize.css'
                    // 'node_modules/css-progress-wizard/css/progress-wizard.min.css'
                ],
                dest: 'dist/css/libs.css'
            },
            dist: {
                src: [
                    'src/**/*.css'
                ],
                dest: 'dist/css/main.min.css'
            }
        },
        uglify: {
            options: {
                compress: true,
                removeComments: true,
                mangle: {
                    reserved: [
                        '$translateProvider',
                        '$stateProvider',
                        '$urlRouterProvider',
                        '$breadcrumbProvider',
                        '$httpProvider',
                        'ezfb'
                    ]
                },
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd/mm/yyyy") %> */\n'
            },
            libs: {
                src: [
                    'node_modules/jquery/dist/jquery.js',
                    'node_modules/tether/dist/js/tether.js',
                    'node_modules/bootstrap/dist/js/bootstrap.js',
                    'node_modules/@fortawesome/fontawesome-free/js/all.js',
                    'node_modules/angular/angular.js',
                    'node_modules/angular-animate/angular-animate.js',
                    'node_modules/angular-breadcrumb/dist/angular-breadcrumb.js',
                    'node_modules/angular-messages/angular-messages.js',
                    'node_modules/angular-sanitize/angular-sanitize.js',
                    'node_modules/angular-toastr/dist/angular-toastr.tpls.js',
                    'node_modules/angular-touch/angular-touch.js',
                    'node_modules/angular-translate/dist/angular-translate.js',
                    'node_modules/@uirouter/angularjs/release/angular-ui-router.js',
                    'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
                    'node_modules/oclazyload/dist/ocLazyLoad.js',
                    'node_modules/angular-xeditable/dist/js/xeditable.js',
                    'node_modules/@priotas/angular-loading-bar/dist/angular-loading-bar.umd.js',
                    'node_modules/moment/moment.js',
                    'node_modules/holderjs/holder.js',
                    'node_modules/jspdf/dist/jspdf.debug.js',
                    'node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.js',
                    // 'node_modules/angular-easyfb/src/angular-easyfb.js',
                    // 'node_modules/loading-bar/index.js'
                ],
                dest: 'dist/js/libs.js'
            },
            dist: {
                src: ['src/app.js', 'src/app.templates.js', 'src/app/**/*.js'],
                dest: 'dist/app.js'
            }
        },
        imagemin:{
            dist: {
                options: {
                    optimizationLevel: 3,
                    progressive: true
                },
                files: [{
                    expand: true,
                    cwd: 'src/img',
                    src: '**/*.{png,jpg,gif}',
                    dest: 'dist/img'
                }]
            }
        },
        express: {
            all: {
                options: {
                    port: 9001,
                    hostname: '0.0.0.0',
                    bases:['dist'],
                    livereload: true
                }
            }
        },
        open: {
            all: {
                path: 'http://localhost:<%= express.all.options.port %>'
            }
        },
        watch: {
            options: {
                livereload: true
            },
            gruntfile: {
                files: ['Gruntfile.js'],
                tasks: ['htmlhint', 'jshint', 'htmlmin:dist', 'ngtemplates', 'copy', 'sass:dist', 'cssmin:dist', 'uglify:dist']
            },
            html: {
                files: ['src/index.html', 'src/**/*.html'],
                tasks: ['htmlhint', 'htmlmin:dist', 'ngtemplates']
            },
            css: {
                files: ['src/sass/*.scss'],
                tasks: ['stylelint:sass', 'sass:dist', 'stylelint:css', 'cssmin:dist']
            },
            js: {
                files: ['Gruntfile.js', 'src/app.js', 'src/app/**/*.js'],
                tasks: ['jshint', 'uglify:dist']
            }
        }
    });

    grunt.registerTask('serve', [
        'htmlhint',
        'jshint',
        'htmlmin:dist',
        'ngtemplates',
        'copy',
        'stylelint:sass',
        'sass:dist',
        'stylelint:css',
        'cssmin',
        'uglify',
        'express',
        'open',
        'watch'
    ]);

    grunt.registerTask('production', [
        'htmlhint',
        'jshint',
        'htmlmin:dist',
        'ngtemplates',
        'copy',
        'stylelint:sass',
        'sass:dist',
        'stylelint:css',
        'cssmin',
        'uglify',
        'imagemin',
        'express',
        'open',
        'watch'
    ]);
};